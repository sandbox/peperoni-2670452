# Field formatter: view
## Description
This field formatter can be used to replace the display of almost any field with a view. Currently supported fields are:

*     "boolean",
*     "changed",
*     "comment",
*     "computed",
*     "created",
*     "datetime",
*     "decimal",
*     "email",
*     "entity_reference",
*     "file",
*     "float",
*     "image",
*     "integer",
*     "language",
*     "link",
*     "list_float",
*     "list_integer",
*     "list_string",
*     "map",
*     "path",
*     "string",
*     "string_long",
*     "text",
*     "text_long",
*     "text_with_summary",
*     "timestamp",
*     "uri",
*     "uuid",
*     "expression_field",

Feel free to post any additional field types to be added to the list.

## Usage

* Download and install the module as usual. No additional configuration is needed.
* Create a view to be used as a field display. This view must have the entity id as first and only parameter.
It can select any field of that entity or any related entity. 
**You should set the caching for the view appropriately!** 
* Go to the /admin/structure/*entity-type*/manage/*bundle-name*/display
* Select "format field as view" as the formatter for the field in question.
* Apply the settings for that formatter by clicking on the gear on the right side. Select the view created in the step above.
* Save the settings by clicking on "update" 
* Don't forget to save all changes by clicking on "Save" at the bottom of the list.

That's all.

## Examples

### Random Image
If you have a list of Images attached to a node and want to display only one of that images randomly (changing every minute), 
you can create a view of "content" like this:

 * Format: **unformatted list**
 * Fields: **Content: Images** 
 Be sure to uncheck *Display all values in the same row* in the *Multiple field settings* section of the field configuration! 
 * Sort **Criteria: Random**
 * Pager: **Display specific number of items | 1 item**
 * More link: **No**
 * Contextual filters: **Content: Node ID**
 * Caching: **Time-based | 1 min/1min**
 
 You can add several displays to the view displaying the image with different styles (it's a good idea to name the display according to the display style)
 
 Now go to the  "Manage display" of the node and apply this view to the images-field. In different display modes you can select different displays of that view and even different views as you like.
 
### Listing children
If you have a relationship from children to the parent, but no relationship from the parent to the children, you can display a list of all children of that parent when displaying the parent.
    
 * Create a view to display all children of a given parent (the parent ID must be the first and only parameter for that view)
 * select any field of your parent entity type (or add a dummy field) to display the children instead. 
 
 You can add a display for counting children, listing children as list, listing children as teasers as you like!
 
### Repeating date formatters
You can use a view to format repeating dates. So you can display a date even if the date is overdue. You can add additional text to the date to mark it overdue and so on.

## Credits
The idea of this module was based on [viewfield] and [views_field_formatter]. 

* With [viewfield] you can define the view to be displayed as a configuration option (with always apply default) or dynamically while editing a node. I always used it with default values and repeated the view fields with different displays to be used with different display modes. Sadly to say that [viewfield] didn't have a Drupal 8 version at time of this writing. 
* With [views_field_formatter] it was possible to add a view as a field formatter, but it unnecessarily used too much parameters and options for the view: the field value itself, the entity id, an implode-string to combine multiple field values, an option to call the view for each occurrence or only once for the field. All these can be handled by the view itself!

So I decided to write this little module to serve my needs.

Enjoy!

[viewfield]: https://www.drupal.org/project/viewfield
[views_field_formatter]: https://www.drupal.org/project/views_field_formatter
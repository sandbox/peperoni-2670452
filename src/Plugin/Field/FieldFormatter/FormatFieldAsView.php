<?php

/**
 * @file
 * Contains \Drupal\field_formatter_view\Plugin\Field\FieldFormatter\FormatFieldAsView.
 */

namespace Drupal\field_formatter_view\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views as Views;

/**
 * Plugin implementation of the 'format_field_as_view' formatter.
 *
 * @FieldFormatter(
 *   id = "format_field_as_view",
 *   label = @Translation("Format field as view"),
 *   field_types = {
 *     "boolean",
 *     "changed",
 *     "comment",
 *     "computed",
 *     "created",
 *     "datetime",
 *     "decimal",
 *     "email",
 *     "entity_reference",
 *     "file",
 *     "float",
 *     "image",
 *     "integer",
 *     "language",
 *     "link",
 *     "list_float",
 *     "list_integer",
 *     "list_string",
 *     "map",
 *     "path",
 *     "string",
 *     "string_long",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *     "timestamp",
 *     "uri",
 *     "uuid",
 *     "expression_field",
 *   }
 * )
 */
class FormatFieldAsView extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'view' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = array();

    $views =  Views::getAllViews();
    $options = array();
    foreach ($views as $view) {
      foreach ($view->get('display') as $display) {
        $options[$view->get('label')][$view->get('id') . '::' . $display['id']] = $view->get('label') . ':' . $display['display_title'] .' (' . $display['id'] . ')';
      }
    }
    if (!empty($options)) {
      $element['view'] = array(
        '#title' => t('View'),
        '#description' => t('Select the view that will be used to display instead. The view must use the entity id as first contextual filter.'),
        '#type' => 'select',
        '#default_value' => $this->getSetting('view'),
        '#options' => $options,
      );
    } else {
      $element['help'] = array(
        '#markup' => '<p>' .t('There are no views in your system.') . '</p>',
      );
    }

    return $element
      + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    list($view, $view_display) = explode('::', $settings['view']);

    if (isset($view)) {
      $summary[] = t('View: @view', array('@view' => $view), array('context' => 'views'));
      $summary[] = t('Display: @display', array('@display' => $view_display), array('context' => 'views'));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    list($view, $view_display) = explode('::', $settings['view'], 2);

    if (!empty($view)) {
      $id = $items->getParent()->getValue()->id();
      $elements[0] = [
        '#type' => 'view',
        '#name' => $view,
        '#display_id' => $view_display,
        '#arguments' => [$id],
      ];
    }

    return $elements;
  }


}
